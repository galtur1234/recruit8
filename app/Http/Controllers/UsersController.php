<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Department; 
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $departments = Department::all();
        return view('users.index', compact('users','departments'));
    }
    public function makeManager($uid) {
        $user = User::findOrFail($uid);
        if (Auth::user()->isManager()) {
            $user->save();
        }
        return back();
    }
    public function remove($uid) {
        $user = User::findOrFail($uid);
        if (Auth::user()->isAdmin()) {
            $user->save();
        }
        return back();
    }
    public function changeDepartment($cid, $sid)
    {
        $user = User::findOrFail($cid);
        $user->department_id = $sid;
        $user->save();

        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $departments = Department::all();
        return view('users.edit', compact('user','departments'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        if(!isset($request->password)){
            $request['password'] = $user->password;   
        }else{
            $request['password'] = Hash::make($request['password']);
        } 
        $user->update($request->all());
        return redirect('users');  
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete(); 
        return redirect('users'); 
    }

}
