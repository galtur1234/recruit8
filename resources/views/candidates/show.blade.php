@extends('layouts.app')

@section('title', 'Candidate')

@section('content')
    @if(Session::has('notallowed'))
        <div class = 'alert alert-danger'>
            {{Session::get('notallowed')}}
        </div>
    @endif
    <h1>Candidate details</h1>
    <table class = "table table-dark">
        <tr>
            <td>Id</td>
            <td>{{$candidate->id}}</td>
        </tr>
        <tr>
            <td>Name</td>
            <td>{{$candidate->name}}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>{{$candidate->email}}</td>
        </tr> 
        <tr>   
            <td>Age</td>
            <td>{{$candidate->age}}</td>
        </tr> 
        <tr>
            <td>Owner</td>
            <td>  
                @if(isset($candidate->owner))
                    {{$candidate->owner->name}}
                @else
                    No owner assigned yet   
                @endif  
            <td>
        </tr>
        <tr>
            <td>Status</td> 
            <td>
                {{$candidate->status->name}}
            </td> 
        </tr>    
        <tr>
            <td>Created</td>
            <td>{{$candidate->created_at}}</td>
        </tr>
        <tr>
            <td>Updated</td>
            <td>{{$candidate->updated_at}}</td> 
         
        </tr>    
    </table>
@endsection
