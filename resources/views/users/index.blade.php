@extends('layouts.app')
@section('content')
@section('title', 'users')
        <h1>Users</h1>
        <table class = "table table-dark">
            <tr> 
                <th>Id</th><th>Name</th><th>Email</th><th>Department</th><th>Created</th><th>Updated</th><th>Edit</th><th>Delete</th>
                @if(Auth::user()->isAdmin())
                    <th>Remove</th>
                @endif
                <th>Make Manager</th>
            </tr>
        @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>

                @if(Auth::user()->isAdmin())
                <td>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Department
                        @if(isset($user->department_id))
                        {{$user->department->name}}  
                      @else
                        Assign owner
                      @endif
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach ($departments as $department)
                        <a class="dropdown-item" href="{{route('users.changedepartment', [$user->id,$department->id])}}">{{$department->name}}</a>
                    @endforeach
                    </div>
                </div>
                </td>
                @else
                    <td>{{$user->department->name}}</td>
                @endif
                </td>
            <td>{{$user->created_at}}</td>
            <td>{{$user->updated_at}}</td>
            <td>
                <a href = "{{route('users.edit',$user->id)}}">Edit</a>
            </td>
            <td>
                <a href = "{{route('user.delete',$user->id)}}">Delete</a> 
            </td> 
            <td>
                @if(Auth::user()->isAdmin() && $user->isManager())
                <a href = "{{route('users.remove',$user->id)}}">Remove</a>
                @endif
            </td>
                       
            <td>
                @if (!$user->isManager())
                   <a href = "{{route('users.makeManager',$user->id)}}">make manager</a>
                   @endif      
               </td>
        </tr>
        @endforeach
        </table>
@endsection
