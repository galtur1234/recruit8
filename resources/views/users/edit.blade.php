@extends('layouts.app')

@section('content')
@section('title', 'users')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit user') }}</div>

                <div class="card-body">
                    <form method = "post" action = "{{action('UsersController@update' ,$user->id)}}">
                        @csrf
                        @METHOD('PATCH') 

                        <div class="form-group" >
                            <label for = "name">User name</label>
                            <input type = "text" class="form-control" name = "name" value = {{$user->name}}>
                        </div> 

                        <div class="form-group">
                            <label for = "email">User email</label>
                            <input type = "text" class="form-control" name = "email" value = {{$user->email}}>
                        </div> 

                        <div class="form-group">
                            <label for = "password">User password</label>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        
                        <div class="form-group">
                            <label for="department_id">User department</label>
                            <div class="col-md-6">
                                {{$user->department->name}}
                            </div>
                            <label for="department_id">If you want to change click here:</label>
                            <div class="col-md-6">
                                <select class="form-control" name="department_id">                                                                         
                                   @foreach ($departments as $department)
                                     <option value="{{ $department->id }}"> 
                                        {{ $department->name }}    
                                     </option>
                                   @endforeach    
                                </select>
                            </div>
                        </div>

                        <div>
                            <input type = "submit" name = "submit" value = "Update user">
                        </div>    
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
